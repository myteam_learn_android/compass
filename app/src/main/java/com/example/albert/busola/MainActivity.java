package com.example.albert.busola;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.List;

import static android.location.Location.FORMAT_DEGREES;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

	TextView azimuthTextView;
	ImageView compassImageView;

	LocationManager locationManager;
	Location latestLocation;

	Context ctx = this;

	private SensorManager sensorManager;

	private Sensor magnetometerSensor;
	private Sensor accelerometerSensor;
	private Sensor gravitySensor;
	private Sensor rotationVectorSensor;

	private GeomagneticField geomagneticField;
	private float magneticDeclination = 0;

	int azimuth;
	int oldAzimuth = 0;

	boolean isTrueHeading = false; // default start with magnetic heading

	Window window;
	WindowManager.LayoutParams layoutParams;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		azimuthTextView = findViewById(R.id.azimuthTextView);
		compassImageView = findViewById(R.id.compassFrontImageView);

		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		magnetometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

		//keep screen on
		window = getWindow();
		layoutParams = window.getAttributes();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		//init location manager and ask for location permissions needed for declination
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		checkLocationPermission();

		//switch from true heading to magnetic heading
		final Button heading = findViewById(R.id.heading);
		heading.setText(getButtonText());

		heading.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				isTrueHeading = !isTrueHeading;

				if (isTrueHeading) {
					if (latestLocation == null) {
						Toast.makeText(ctx, "We really need location permission or a location to be set", Toast.LENGTH_LONG).show();
						isTrueHeading = false;
						return;
					}
				}

				heading.setText(getButtonText());

				//inited here to have the latest location available
				if (isTrueHeading) {
					if (geomagneticField == null) //init only once
					{
						float degreeLatitude = Float.parseFloat(Location.convert(latestLocation.getLatitude(), FORMAT_DEGREES));
						float degreeLongitude = Float.parseFloat(Location.convert(latestLocation.getLongitude(), FORMAT_DEGREES));
						float altitude = (float) latestLocation.getAltitude();

						//geomagnetic field
						geomagneticField = new GeomagneticField(degreeLatitude, degreeLongitude, altitude, System.currentTimeMillis());
						magneticDeclination = geomagneticField.getDeclination();
					}

					//probably not necessary since the listeners belows are triggered very often
//					azimuth = azimuth + (int) magneticDeclination;
//					azimuthTextView.setText( String.valueOf( azimuth ) + "°" );
				}
			}
		});
	}

	private String getButtonText() {

		if (isTrueHeading) {
			return "This is TRUE HEADING (with declination). \r\n Click to change!!!";
		} else {
			return "This is MAGNETIC HEADING (without declination)\r\n Click to change!!!";
		}
	}

	int operationMode;

	@Override
	protected void onResume() {
		super.onResume();

		if (rotationVectorSensor != null) {

			sensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_NORMAL);

			operationMode = 2;
		} else if (gravitySensor != null && magnetometerSensor != null) {

			sensorManager.registerListener(this, magnetometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
			sensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_NORMAL);

			operationMode = 1;
		} else if (magnetometerSensor != null && accelerometerSensor != null) {

			sensorManager.registerListener(this, magnetometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
			sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);

			operationMode = 0;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	float[] magneticField = new float[3];
	float[] acceleration = new float[3];
	float[] gravity = new float[3];

	float[] rotationMatrix = new float[9];
	float[] inclinationMatrix = new float[9];
	float[] orientation = new float[3];

	float[] rotationVector = new float[5];

	ObjectAnimator rotationAnimation;

	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {

		switch (sensorEvent.sensor.getType()) {
			case Sensor.TYPE_MAGNETIC_FIELD:
				System.arraycopy(sensorEvent.values, 0, magneticField, 0, sensorEvent.values.length);
				break;
			case Sensor.TYPE_ACCELEROMETER:
				System.arraycopy(sensorEvent.values, 0, acceleration, 0, sensorEvent.values.length);
				break;
			case Sensor.TYPE_GRAVITY:
				System.arraycopy(sensorEvent.values, 0, gravity, 0, sensorEvent.values.length);
				break;
			case Sensor.TYPE_ROTATION_VECTOR:
				System.arraycopy(sensorEvent.values, 0, rotationVector, 0, sensorEvent.values.length);
				break;
		}

		if (operationMode == 0 || operationMode == 1) {

			if (SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix, (operationMode == 0) ? acceleration : gravity, magneticField)) {

				float azimuthRad = SensorManager.getOrientation(rotationMatrix, orientation)[0]; //azimuth in radians
				double azimuthDeg = Math.toDegrees(azimuthRad); // azimuth in degrees; value from -180 to 180

				azimuth = ((int) azimuthDeg + 360) % 360; //convert -180/180 to 0/360
				if (isTrueHeading) {
					azimuth = azimuth + (int) magneticDeclination;
				}
			}
		} else {

			// calculate the rotation matrix
			SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationVector);
			// get the azimuth value (orientation[0]) in degree
			azimuth = (int) (Math.toDegrees(SensorManager.getOrientation(rotationMatrix, orientation)[0]) + 360) % 360;
			if (isTrueHeading) {
				azimuth = azimuth + (int) magneticDeclination;
			}
		}

		azimuthTextView.setText(String.valueOf(azimuth) + "°");
		//compassImageView.setRotation(-azimuth); //set orientation without animation

		//set rotation with animation

		float tempAzimuth;
		float tempCurrentAzimuth;

		if (Math.abs(azimuth - oldAzimuth) > 180) {
			if (oldAzimuth < azimuth) {
				tempCurrentAzimuth = oldAzimuth + 360;
				tempAzimuth = azimuth;
			} else {
				tempCurrentAzimuth = oldAzimuth;
				tempAzimuth = azimuth + 360;
			}
			rotationAnimation = ObjectAnimator.ofFloat(compassImageView, "rotation", -tempCurrentAzimuth, -tempAzimuth);
			rotationAnimation.setDuration(250);
			rotationAnimation.start();
		} else {
			rotationAnimation = ObjectAnimator.ofFloat(compassImageView, "rotation", -oldAzimuth, -azimuth);
			rotationAnimation.setDuration(250);
			rotationAnimation.start();
		}
		oldAzimuth = azimuth;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}

	/**************ACTION BAR MENU************************/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.action_bar, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.help:

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
				alertDialogBuilder.setTitle("Compass");
				alertDialogBuilder.setMessage("This is Compass app, part of the ITAcademy and LinkAcademy Android Development Program.\n\nAuthor: Vladimir Dresevic, Link Group");

				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();

				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	//------- check location permissions, needed for the magnetic declination-----
	public static final int MY_PERMISSIONS_REQUEST_LOCATION = 2020;

	public boolean checkLocationPermission() {
		if (ContextCompat.checkSelfPermission(this,
				Manifest.permission.ACCESS_FINE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {

			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this,
					Manifest.permission.ACCESS_FINE_LOCATION)) {

				new AlertDialog.Builder(this)
						.setTitle("Avem nevoie de permisiunea de locatie pt declinatia magnetica")
						.setMessage("please....")
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								//Prompt the user once explanation has been shown
								ActivityCompat.requestPermissions(MainActivity.this,
										new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
										MY_PERMISSIONS_REQUEST_LOCATION);
							}
						})
						.create()
						.show();
			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this,
						new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
						MY_PERMISSIONS_REQUEST_LOCATION);
			}
			return false;
		} else {
			latestLocation = getLastKnownLocation();
			return true;
		}
	}

	private Location getLastKnownLocation() {
		List<String> providers = locationManager.getProviders(true);
		Location bestLocation = null;
		for (String provider : providers) {
			Location l = locationManager.getLastKnownLocation(provider);
			if (l == null) {
				continue;
			}
			if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
				// Found best last known location: %s", l);
				bestLocation = l;
			}
		}
		return bestLocation;
	}



	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {


		switch( requestCode )
		{
			case MY_PERMISSIONS_REQUEST_LOCATION:
			{
				// If request is cancelled, the result arrays are empty.
				if( grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED )
				{

					// permission was granted,
					if( ContextCompat.checkSelfPermission( this,
							Manifest.permission.ACCESS_FINE_LOCATION )
							== PackageManager.PERMISSION_GRANTED )
					{

						latestLocation = getLastKnownLocation();
					}
				}
				else
				{
					//permission denied
					Toast.makeText( ctx, "We really need location permission. Please", Toast.LENGTH_LONG ).show();



				}
				return;
			}
		}
	}
}
